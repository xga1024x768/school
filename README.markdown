学校で作った提出課題などを入れておくリポジトリです。

# ディレクトリ構成
* clang\_and\_algorithms/
	* C言語とアルゴリズムの授業で作ったファイル
* clang\_and\_algorithms/exam/
	* 試験で書いたソース
* clang\_and\_algorithms/exercise/
	* 実習課題
* clang\_and\_algorithms/textbook/
	* 教科書のプログラミング問題

# Makefileについて
gccが入っていればmakeコマンドでソースを全て一気にコンパイルできます。

