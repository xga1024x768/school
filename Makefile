include Makefile.common

subdirs=clang_and_algorithms

default:
	@for file in ${subdirs}; do $(MAKE) -C $$file; done

clean:
	@for file in ${subdirs}; do $(MAKE) clean -C $$file; done

