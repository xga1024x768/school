// 2013-11-27 後期中間試験 option1,2,3,4

#define array_size(array) sizeof(array)/sizeof(array[0])

#define swap(type,a,b) { \
		type tmp; \
		tmp = a; \
		a = b; \
		b = tmp; \
	}

#define ASC  1
#define DESC 0

/*
 * 部品の情報が入った構造体
 */
const struct PartsInfo
{
	// 部品コード
	const int code;
	// 部品名
	const char* name;
} PartsInfo[] = {
	{ 1, "コンボドライブ"},
	{ 2, "メモリ"},
	{ 3, "ハードディスク"},
	{ 4, "キーボード"},
	{ 5, "液晶モニター"},
	{ 6, "サウンドボード"},
	{ 7, "スピーカー"},
	{ 8, "パワー[ATX電源]"},
	{ 9, "PC・LAN"},
	{10, "PCボードパーツ"},
};

/*
 * 部品の在庫数を記録する構造体
 */
struct PartsCount
{
	// どの部品か
	const struct PartsInfo* parts;
	// 個数
	int count;
};

/*
 * 部品の数を記録する構造体の配列を整列する
 *
 * 引数
 * parts_count: 部品の数を記録する構造体の配列
 * coutnlen;    配列の大きさ
 * asc:         昇順の場合ASC、降順の場合DESC
 */
void sort(struct PartsCount parts_count[], int countlen, int asc)
{
	int i, j, k;

	for (i = 0; i < countlen; i++)
	{
		k = i;
		for (j = i+1; j < countlen; j++)
		{
			if (asc && parts_count[j].count < parts_count[k].count)
			{
				k = j;
			}
			else if (!asc && parts_count[j].count > parts_count[k].count)
			{
				k = j;
			}
		}
		swap(struct PartsCount, parts_count[i], parts_count[k]);
	}
}
