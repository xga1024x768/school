// 2013-11-27 後期中間試験 option1,2,3,4

#include <stdio.h>
#include "2013-11-27.h"

/*
 * 指定した部品コードの情報を取得する
 *
 * 引数
 * code: 部品コード
 *
 * 戻り値
 * 部品コードが存在した場合はその部品の情報を、存在しない場合NULLを返す
 */
const struct PartsInfo* find_partsinfo(int code)
{
	int i;

	for (i = 0; i < array_size(PartsInfo); i++)
	{
		if (PartsInfo[i].code == code)
		{
			return &PartsInfo[i];
		}
	}

	return NULL;
}

#define ARRAYSIZE_TOO_SHORT (const struct PartsInfo*)-1

/*
 * 指定した部品コードの在庫数を登録する
 *
 * 引数
 * parts_count: 部品の数を記録する構造体の配列
 * coutnlen;    配列の大きさ
 * code:        部品コード
 * count:       個数
 *
 * 戻り値
 * 部品コードが存在した場合はその部品の情報を、存在しない場合NULLを返す。
 * また、parts_countに部品が登録されておらず、部品を登録する領域も足りないときはARRAYSIZE_TOO_SHORTを返す。
 */
const struct PartsInfo* add_parts(struct PartsCount parts_count[], int countlen, int code, int count)
{
	int i;

	for (i = 0; i < countlen && parts_count[i].parts; i++)
	{
		if (parts_count[i].parts->code == code) break;
	}

	// parts_countに部品が登録されておらず、大きさも足りずこれ以上の部品を登録できなかった
	if (i >= countlen) return ARRAYSIZE_TOO_SHORT;

	if (parts_count[i].parts == NULL)
	{
		// 部品が登録されていなかったので登録する
		const struct PartsInfo* pinfo = find_partsinfo(code);
		if (!pinfo)
		{
			// 指定された部品コードが存在しなかった
			return NULL;
		}

		parts_count[i].parts = pinfo;
		parts_count[i].count = count;
	}
	else if (parts_count[i].parts->code == code)
	{
		// 既に部品が登録されていたので上書きする
		parts_count[i].count = count;
	}

	return parts_count[i].parts;
}

/*
 * 在庫一覧表を出力
 *
 * 引数
 * parts_count: 部品の数を記録する構造体の配列
 * coutnlen;    配列の大きさ
 */
void show_stocklist(struct PartsCount parts_count[], int countlen)
{
	int i;

	printf("在庫一覧表\n");
	printf("--------------------------------\n");
	for (i = 0; i < countlen; i++)
	{
		if (parts_count[i].count > 0)
		{
			printf("%-16s %d\n", parts_count[i].parts->name, parts_count[i].count);
		}
	}
}

int main()
{
	int code, count;
	struct PartsCount parts_count[array_size(PartsInfo)] = {0};
	const struct PartsInfo* pinfo;
	
	printf("部品番号と個数を入力 (^Zで終了)\n");
	while (1)
	{
		if (scanf("%d %d", &code, &count) == EOF) break;
		pinfo = add_parts(parts_count, array_size(parts_count), code, count);
		if (pinfo)
		{
			if (pinfo == ARRAYSIZE_TOO_SHORT)
			{
				printf("エラー: ARRAYSIZE_TOO_SHORT\n");
			}
			else
			{
				printf("%s(部品コード:%d)を%d個登録しました。\n", pinfo->name, pinfo->code, count);
			}
		}
		else
		{
			printf("そんな部品はありません。\n");
		}
	}
	
	sort(parts_count, array_size(parts_count), DESC);
	show_stocklist(parts_count, array_size(parts_count));

	return 0;
}

// 実行結果
/*
部品番号と個数を入力 (^Zで終了)
2
3
メモリ(部品コード:2)を3個登録しました。
3
5
ハードディスク(部品コード:3)を5個登録しました。
4
4
キーボード(部品コード:4)を4個登録しました。
5
8
液晶モニター(部品コード:5)を8個登録しました。
32
1
そんな部品はありません。
^Z
在庫一覧表
--------------------------------
液晶モニター     8
ハードディスク   5
キーボード       4
メモリ           3
続行するには何かキーを押してください . . .
*/
