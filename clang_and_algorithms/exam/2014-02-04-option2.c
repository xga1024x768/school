// 2014-02-04 後期期末試験 オプション2

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#define snprintf _snprintf
#define array_size(a) (sizeof(a)/sizeof(a[0]))

void abort_with_text(char* format, ...)
{
	va_list ap;
	va_start(ap, format);
	vfprintf(stderr, format, ap);
	va_end(ap);
	exit(1);
}

struct CountryGNI
{
	char         name[64];	// 国名
	unsigned int gni;
};

int load_file(char* filename, struct CountryGNI* gnis, int gnis_size)
{
	int record_cnt;
	char line[128];
	FILE* fp = fopen(filename, "r");
	if (!fp) abort_with_text("ファイル \"%s\" が開けませんでした。\n", filename);
	
	record_cnt = 0;
	while (fgets(line, 128, fp) != NULL)
	{
		sscanf(line, "%s %d", gnis[record_cnt].name, &gnis[record_cnt].gni);
		record_cnt++;
	}
	
	return record_cnt;
}

void sort_by_gni(struct CountryGNI* gnis, int gnis_size)
{
	struct CountryGNI* left;
	struct CountryGNI* right;
	int left_cnt, right_cnt;
	int i, j, k;

	if (gnis_size == 1) return;

	left_cnt  = gnis_size/2;
	right_cnt = gnis_size-gnis_size/2;

	left  = (struct CountryGNI*)malloc(left_cnt  * sizeof(struct CountryGNI));
	if (!left)  abort_with_text("メモリを確保できませんでした。\n");
	right = (struct CountryGNI*)malloc(right_cnt * sizeof(struct CountryGNI));
	if (!right) abort_with_text("メモリを確保できませんでした。\n");

	memcpy(left,  gnis,          left_cnt  * sizeof(struct CountryGNI));
	memcpy(right, gnis+left_cnt, right_cnt * sizeof(struct CountryGNI));

	sort_by_gni(left,  left_cnt);
	sort_by_gni(right, right_cnt);

	i = j = k = 0;
	while (i < left_cnt && j < right_cnt)
	{
		if (left[i].gni > right[j].gni)
		{
			gnis[k++] = left[i++];
		}
		else
		{
			gnis[k++] = right[j++];
		}
	}

	while (i < left_cnt)
	{
		gnis[k++] = left[i++];
	}

	while (j < right_cnt)
	{
		gnis[k++] = right[j++];
	}

	free(left);
	free(right);
}

void separated_number_s(char* str, int bufsize, unsigned int number)
{
	int i;
	int splast;
	char buf[32];
	int spnums[4] = {0};

	for (i = 0; number > 0; i++)
	{
		spnums[i] = number%1000;
		number /= 1000;
	}
	
	for (i = 3; i >= 0; i--)
	{
		if (spnums[i] != 0)
		{
			splast = i;
			break;
		}
	}

	if (splast == -1) strcpy(str, "0");
	else
	{
		sprintf(str, "%d", spnums[i]);
		for (i = splast-1; i >= 0; i--)
		{
			sprintf(str, "%s,%03d", str, spnums[i]);
		}
	}
}

void show_countries_search(struct CountryGNI* gnis, int gnis_size, char c[3])
{
	int i;
	char number_s[64];
	for (i = 0; i < gnis_size; i++)
	{
		if (strncmp(gnis[i].name, c, 2) == 0)
		{
			separated_number_s(number_s, 64, gnis[i].gni);
			printf("%3d %-20s %s\n", i+1, gnis[i].name, number_s);
		}
	}
}

int main()
{
	struct CountryGNI gnis[200];
	int record_cnt;
	char c[3];

	printf("国名の最初の一文字を入力: ");
	scanf("%2s", c);

	record_cnt = load_file("gin.txt", gnis, array_size(gnis));
	sort_by_gni(gnis, record_cnt);
	printf("世界の平均年収順位 2013年WHO発表\n");
	printf("--------------------------------\n");
	show_countries_search(gnis, record_cnt, c);
	return 0;
}
