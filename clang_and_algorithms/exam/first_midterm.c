#include <stdio.h>
 
// 正しい学籍Noかどうか
#define is_valid_number(number) ((number) >= 1 && (number) <= 100)
// 正しい得点かどうか
#define is_valid_score(score) ((score) >= 0 && (score) <= 100)
// 入力が正しいかどうか
#define is_valid_input(number, math, english, japanese) (is_valid_number(number) && \
	is_valid_score(math) && is_valid_score(english) && is_valid_score(japanese))
// 入力チェックエラーメッセージ
#define show_error_message(which) printf(which "がおかしいです。¥n")
// 今までで一番得点が高い人であればその教科の学籍Noと最大得点を書き換えるマクロ
#define best_scorer(subject) if (subject > best_##subject) { \
		num_##subject = number; \
		best_##subject = subject; \
	}
 
// get_level
// 引数　: それぞれの教科の得点
// 返り値: レベルのアルファベット
char get_level(int math, int english, int japanese)
{
	char level = 0;
	int sum = math + english + japanese;    // 合計点数
	if      (sum >= 270) level = 'A';
	else if (sum >= 240) level = 'B';
	else if (sum >= 210) level = 'C';
	else if (sum >= 180) level = 'D';
	else		 level = 'E';
	return level;
}
 
int main()
{
	int number, math, english, japanese;    // 入力された学籍Noと数学、英語、国語の得点
	unsigned int count = 0;				 // 入力されたデータの個数
	int sum_math  = 0, sum_english  = 0, sum_japanese  = 0; // 全学生の得点の合計
	int best_math = 0, best_english = 0, best_japanese = 0; // これまで入力された中で最大の得点
	int num_math  = 0, num_english  = 0, num_japanese  = 0; // 最大得点者の学籍番号
 
	printf("成績評価をします。学籍No、3教科[数学、英語、国語]の得点を入力してください¥n");
	printf("例【1 85 75 90】終了はctrl+z入力¥n");
	while (scanf("%d %d %d %d", &number, &math, &english, &japanese) != EOF)
	{
		// 入力をチェックしメッセージを表示
		if (!is_valid_input(number, math, english, japanese))
		{
			if (!is_valid_number(number))  show_error_message("学生No");
			if (!is_valid_score(math))     show_error_message("数学の得点");
			if (!is_valid_score(english))  show_error_message("英語の得点");
			if (!is_valid_score(japanese)) show_error_message("国語の得点");
			continue;
		}
 
		// 入力は正しかったので評価を表示
		printf("¥t評価: %c¥n", get_level(math, english, japanese));
	       
		// 最大得点者用の処理を行う
		best_scorer(math);
		best_scorer(english);
		best_scorer(japanese);
 
		// 今までの得点の合計を計算しておく
		sum_math     += math;
		sum_english  += english;
		sum_japanese += japanese;
 
		count++;
		rewind(stdin);
	}
	if (count > 0)
	{
		// 少なくとも1つは成績の入力があったので平均点と最大得点者を表示する
		printf("平均点¥n");
		printf("数学:%.2lf点、英語:%.2lf点、国語:%.2lf点¥n",
			(double)sum_math/count,
			(double)sum_english/count,
			(double)sum_japanese/count
		);
 
		printf("科目毎の最大得点者¥n");
		printf("数学: %d¥n英語: %d¥n国語: %d¥n",
			num_math, num_english, num_japanese
		);
	}
}
 
// 実行結果
/*
成績評価をします。学籍No、3教科[数学、英語、国語]の得点を入力してください
例【1 85 75 90】終了はctrl+z入力
1 85 95 90
	評価: A
2 65 80 70
	評価: C
3 78 88 85
	評価: B
4 60 55 70
	評価: D
0 78 99 23
学生Noがおかしいです。
5 120 -50 99
数学の得点がおかしいです。
英語の得点がおかしいです。
^Z
平均点
数学:72.00点、英語:79.50点、国語:78.75点
科目毎の最大得点者
数学: 1
英語: 1
国語: 1
*/
