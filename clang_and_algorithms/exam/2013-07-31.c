// 2013-07-31 前期期末試験 アルバイトの給料計算
#include <stdio.h>

// アルバイトの最大数
#define MAX_WORKER 10

#define array_size(array) sizeof(array)/sizeof(array[0])

struct WorkTime
{
	int hour;
	int minute;
};

// アルバイトの情報
struct Worker
{
	int number;
	char level;
	struct WorkTime start;
	struct WorkTime finish;
};

// 働いた時間を分で返す
// 日を跨いで働いた場合の処理をしていないので注意
int calculate_working_minute(struct WorkTime* start, struct WorkTime* finish)
{
	return (finish->hour*60 + finish->minute) - (start->hour*60 + start->minute);
}

// 給料を計算する
int calculate_pay(const int hourly_payments[], struct Worker* worker)
{
	int pay;
	int work_min;
	work_min = calculate_working_minute(&worker->start, &worker->finish);
	pay = (work_min/10) * (hourly_payments[worker->level-'a']/6);
	return pay;
}

// アルバイトの情報を入力する関数
// 入力されたデータはworkerに代入される
// EOFが入力された場合偽を返す
char input_worker_info(struct Worker* worker)
{
	return scanf(
		"%d %c %d %d %d %d",
		&worker->number, &worker->level,
		&worker->start.hour, &worker->start.minute,
		&worker->finish.hour, &worker->finish.minute
		) != EOF;
}

int main()
{
	// レベルに応じた時給の入った配列
	static const int hourly_payments[] = {
	// level:  a    b    c    d    e
	          720, 780, 840, 900, 960
	};
	// アルバイトの情報が入った構造体の配列
	struct Worker worker[MAX_WORKER];
	// 入力されたアルバイトの人数
	int worker_count;
	// アルバイト全員の給料の合計
	int sum_pay;

	int pay;
	int i;

	printf("給料計算をします。アルバイトNo、レベル、開始時刻、終了時間を入力してください。\n");
	printf("例【1 a 9 0 15 35】終了は^Z\n");
	i = 0;
	while (i < MAX_WORKER)
	{
		if (!input_worker_info(&worker[i])) break;

		// アルバイトNoの入力チェック
		if (worker[i].number < 1 || worker[i].number > 100)
		{
			printf(" 入力エラー: %d\n", worker[i].number);
			continue;
		}

		// レベルの入力チェック
		if (worker[i].level < 'a' || worker[i].level > 'a'+(array_size(hourly_payments)-1))
		{
			printf(" 入力エラー: %c\n", worker[i].level);
			continue;
		}

		i++;
	}
	// アルバイトの人数をメモしておく
	worker_count = i;

	// アルバイト毎の給料とその合計を表示
	sum_pay = 0;
	for (i = 0; i < worker_count; i++)
	{
		pay = calculate_pay(hourly_payments, &worker[i]);
		printf("%-4d%d円\n", worker[i].number, pay);
		sum_pay += pay;
	}
	printf("総合計金額:%d円\n", sum_pay);

	return 0;
}

// 実行結果
/*
給料計算をします。アルバイトNo、レベル、開始時刻、終了時間を入力してください。
例【1 a 9 0 15 35】終了は^Z
1 a 15 0 20 30
2 b 12 10 15 8
3 c 13 0 19 0
4 f 9 0 13 30
 入力エラー: f
4 d 7 11 11 13
5 e 8 0 16 1
^Z
1   3960円
2   2210円
3   5040円
4   3600円
5   7680円
総合計金額:22490円
続行するには何かキーを押してください . . .
*/
