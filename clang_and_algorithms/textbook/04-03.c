#include <stdio.h>

void star(int count)
{
	for (; count >= 0; count--) printf("* ");
	printf("\n");
}

int main()
{
	int num, i;
	printf("三角形の一辺を入力してください:");
	scanf("%d", &num);
	for (i = 0; i < num; i++) star(i);
	return 0;
}

