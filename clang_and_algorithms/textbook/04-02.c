#include <stdio.h>

int sum = 0;

void j_heikin(int count)
{
	printf("入力した数値の平均は%dです。\n", sum/count);
}

int main()
{
	int num, count;
	printf("数値を複数入力してください (終了^Z)\n");
	for(count = 0; scanf("%d", &num) != EOF; count++) sum += num;
	j_heikin(count);
	return 0;
}

