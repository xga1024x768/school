#include <stdio.h>

int main()
{
	char c1, c2;
	char num;
	printf("2桁の16進数 ('41'〜'49'、'50'〜'59') を入力してください:");
	scanf("%c%c", &c1, &c2);
	num = (c1-'0') * 16 + (c2-'0');
	printf("文字コード「%x」の文字は「%c」です。\n", num, num);
	return 0;
}

