#include <stdio.h>

int main()
{
	int num1, num2, tmp, n, nn;
	int numlen, linelen;
	int left, digitnum, digit, x;
	printf("整数を二つ入力してください:");
	scanf("%d %d", &num1, &num2);
	if (num1 > num2)
	{
		tmp = num1;
		num1 = num2;
		num2 = tmp;
	}
	for (nn = n = num1, linelen = 0; n <= num2; n=++nn)
	{
		if (n == 0)
		{
			printf("0");
			if (++linelen == 10)
			{
				printf("\n");
				linelen = 0;
			}
			printf(",");
			if (++linelen == 10)
			{
				printf("\n");
				linelen = 0;
			}
		}
		else
		{
			if (n < 0)
			{
				printf("-");
				if (++linelen == 10)
				{
					printf("\n");
					linelen = 0;
				}
				n *= -1;
			}
			if (n <= 9)         numlen = 1;
			else if (n <= 99)   numlen = 2;
			else if (n <= 999)  numlen = 3;
			else if (n <= 9999) numlen = 4;
			else                numlen = 5;
			left = n;
			for (x = 1, digit = 1; x < numlen; x++) digit *= 10;
			do
			{
				digitnum = left/digit;
				left -= digitnum * digit;
				digit /= 10;
				printf("%d", digitnum);
				if (++linelen == 10)
				{
					printf("\n");
					linelen = 0;
				}
			} while(digit != 0);
			if (n != num2)
			{
				printf(",");
				if (++linelen == 10)
				{
					printf("\n");
					linelen = 0;
				}
			}
		}
	}
	return 0;
}

