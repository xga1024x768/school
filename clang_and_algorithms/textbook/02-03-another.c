#include <stdio.h>

int main()
{
	int num;
	printf("2桁の16進数 ('41'〜'49'、'50'〜'59') を入力してください:");
	scanf("%d", &num);
	num = (num/10) * 16 + (num%10);
	printf("文字コード「%x」の文字は「%c」です。\n", num, num);
	return 0;
}

