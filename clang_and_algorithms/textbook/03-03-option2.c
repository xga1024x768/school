#include <stdio.h>

int main()
{
	int c;
	int pc = 'N';	// Previous Character
	unsigned int digit;
	int num;
	printf("データを入力してください:");
	while ((c = getchar()) != '\n')
	{
		switch (c)
		{
			case 'T':
				putchar('\t');
				break;
			case 'N':
				putchar('\n');
				break;
			case '-':
				printf("%d", (num/digit/10)*120);
				putchar('-');
				break;
			case '+':
				printf("%d", (num/digit/10)*120);
				putchar(' ');
				break;
			case '$':
				num = 0;
				digit = 10000;
				putchar('\\');
				break;
			default:
				if (pc == 'N' && c >= 'a' && c <= 'z')
				{
					putchar(c-0x20);
				}
				else if (c >= '0' && c <= '9')
				{
					num += (c-'0') * digit;
					digit /= 10;
				}
				else
				{
					putchar(c);
				}
				break;
		}
		pc = c;
	}
	return 0;
}

