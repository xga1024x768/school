#include <stdio.h>

int main()
{
	int num1, num2, tmp, i;
	printf("整数を二つ入力してください:");
	scanf("%d %d", &num1, &num2);
	if (num1 > num2)
	{
		tmp = num1;
		num1 = num2;
		num2 = tmp;
	}
	for (i = 0; num1+i <= num2; i++)
	{
		printf("%d", num1+i);
		if ((i+1)%10 == 0)
		{
			printf("\n");
		}
		else if (num1+i < num2)
		{
			printf(",");
		}
	}
	return 0;
}

