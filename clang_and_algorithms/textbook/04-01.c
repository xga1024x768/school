#include <stdio.h>

int keisan(int num1, int num2)
{
	return num1 + num2;
}

int main()
{
	int num1, num2, num3;
	printf("数字を三つ入力してください:");
	scanf("%d %d %d", &num1, &num2, &num3);
	printf("%d + %d = %d\n", num1, num2, keisan(num1, num2));
	printf("%d + %d = %d\n", num2, num3, keisan(num2, num3));
	printf("%d + %d = %d\n", num3, num1, keisan(num3, num1));
	return 0;
}

