#include <stdio.h>

int main()
{
	int c;
	int pc = 'N';	// Previous Character
	printf("データを入力してください:");
	while ((c = getchar()) != '\n')
	{
		switch (c)
		{
			case 'T':
				putchar('\t');
				break;
			case 'N':
				putchar('\n');
				break;
			case '-':
				printf("00");
				putchar('-');
				break;
			case '+':
				printf("00");
				putchar(' ');
				break;
			case '$':
				putchar('\\');
				break;
			default:
				if (pc == 'N' && c >= 'a' && c <= 'z')
				{
					putchar(c-0x20);
				}
				else
				{
					putchar(c);
				}
				break;
		}
		pc = c;
	}
	return 0;
}

