#include <stdio.h>

int main()
{
	int num, i, x;
	do
	{
		printf("数字 (2〜9) を入力してください:");
		scanf("%d", &num);
		if (num >= 2 && num <= 9) break;
		rewind(stdin);
		printf("入力エラーです!!\n");
	} while(1);

	for (i = 0; i < num; i++) printf("* ");
	printf("\n");

	for (i = 2; i < num; i++)
	{
		printf("* ");
		for (x = 2; x < num; x++) printf("  ");
		printf("* ");
		printf("\n");
	}

	for (i = 0; i < num; i++) printf("* ");
	printf("\n");

	return 0;
}

