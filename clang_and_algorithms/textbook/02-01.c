#include <stdio.h>

int main()
{
	int num1, num2;
	printf("数字を二つ入力してください:");
	scanf("%d %d", &num1, &num2);
	printf(
		"%d + %d = %d\t%d - %d = %d\n",
		num1, num2, num1 + num2,
		num1, num2, num1 - num2
	);
	printf(
		"%d * %d = %d\t%d / %d = %d\n",
		num1, num2, num1 * num2,
		num1, num2, num1 / num2
	);
	return 0;
}

