#include <stdio.h>

void star(int count)
{
	if (count > 0)
	{
		star(count - 1);
		for (count--; count >= 0; count--) printf("* ");
		printf("\n");
	}
}

int main()
{
	int num, i;
	printf("三角形の一辺を入力してください:");
	scanf("%d", &num);
	star(num);
	return 0;
}

