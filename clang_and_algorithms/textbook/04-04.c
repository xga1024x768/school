#include <stdio.h>

int gcm(int x, int y)
{
	int mod;
	while ((mod=x%y) != 0)
	{
		x = y;
		y = mod;
	}
	return y;
}

int main()
{
	int num1, num2;
	printf("数字を二つ入力してください:");
	scanf("%d %d", &num1, &num2);
	printf("最大公約数は%dです\n", gcm(num1, num2));
	return 0;
}

