#include <stdio.h>

int main()
{
	int c;
	printf("データを入力してください:");
	while ((c = getchar()) != '\n')
	{
		switch (c)
		{
			case 'T':
				putchar('\t');
				break;
			case 'N':
				putchar('\n');
				break;
			case '+':
				putchar(' ');
				break;
			default:
				putchar(c);
				break;
		}
	}
	return 0;
}

