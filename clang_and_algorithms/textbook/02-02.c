#include <stdio.h>

int main()
{
	char c;
	printf("英小文字を入力してください:");
	scanf("%c", &c);
	printf("入力した英小文字は\"%c\"で、大文字にすると\"%c\"です。\n", c, c-0x20);
	return 0;
}

