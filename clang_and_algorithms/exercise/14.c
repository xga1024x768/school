// 2013-05-29 提出課題 実習課題14 和暦→西暦変換
#include <stdio.h>

#define NENGO_MEIJI  'M'	// 明治用の記号
#define NENGO_TAISHO 'T'	// 大正用の記号
#define NENGO_SHOWA  'S'	// 昭和用の記号
#define NENGO_HEISEI 'H'	// 平成用の記号

#define YEAR_MEIJI  1868	// 明治の開始年
#define YEAR_TAISHO 1912	// 大正の開始年
#define YEAR_SHOWA  1926	// 昭和の開始年
#define YEAR_HEISEI 1989	// 平成の開始年

#define INVALID_NENGO -1	// 年号がおかしい
#define INVALID_YEAR  -2	// 年がおかしい
#define INVALID_MONTH -3	// 月がおかしい
#define INVALID_DAY   -4	// 日がおかしい

// 英小文字→英大文字変換; 英小文字以外はそのまま返す
#define uppercase(c) c>='a'&&c<='z' ? c-0x20 : c

// うるう年の判定
#define isLeapYear(year) ((year%4==0) && (year%100!=0)) || (year%400==0)

// checkDay関数	日付が指定した範囲内か確認
// 返り値: INVALID_YEAR:年が範囲外 INVALID_MONTH:月が範囲外 INVALID_DAY:日が範囲外 0:範囲内
// 引数:   year,month,day:確認対象の日付 year1,month1,day1:範囲の開始の日付 year2,month2,day2:範囲の終了の日付
int checkDay(int year, int month, int day, int year1, int month1, int day1, int year2, int month2, int day2) {
	if (year < year1) return INVALID_YEAR;
	if (year == year1 && month < month1) return INVALID_MONTH;
	if (year == year1 && month == month1 && day < day1) return INVALID_DAY;
	if (year > year2) return INVALID_YEAR;
	if (year == year2 && month > month2) return INVALID_MONTH;
	if (year == year2 && month == month2 && day > day2) return INVALID_DAY;
	return 0;
}

// getYear関数	和暦を西暦に変換
// 返り値: INVALID_NENGO: エラー (年号の記号がおかしい) INVALID_YEAR: エラー (年がおかしい)
// 　　　  INVALID_MONTH: エラー (月がおかしい)　　　　 INVALID_DAY : エラー (日がおかしい)
// 　　　  その他: 西暦での年
// 引数:   nengo: 年号 year: 和暦 month: 月 day: 日
int getYear(char nengo, int year, int month, int day) {
	// 1月当たりの日数 (2月はうるう年でない場合の日数)
	static const int dpm[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	int tmp;
	int start = 0;  // 指定された元号の西暦での開始年
	int gyear;	  // 変換後の西暦

	switch (nengo) {
		case NENGO_MEIJI:
			// 01-10-23から45-07-30まで
			if ((tmp = checkDay(year, month, day, 1, 10, 23, 45,  7, 30)) != 0) return tmp;
			start = YEAR_MEIJI;
			break;
		case NENGO_TAISHO:
			// 01-07-30から15-12-25まで
			if ((tmp = checkDay(year, month, day, 1,  7, 30, 15, 12, 25)) != 0) return tmp;
			start = YEAR_TAISHO;
			break;
		case NENGO_SHOWA:
			// 01-12-25から64-01-07まで
			if ((tmp = checkDay(year, month, day, 1, 12, 25, 64,  1,  7)) != 0) return tmp;
			start = YEAR_SHOWA;
			break;
		case NENGO_HEISEI:
			// 01-01-08から
			if (year == 1 && month == 1 && day < 8) return INVALID_DAY;
			start = YEAR_HEISEI;
			break;
		default: return -1;
	}
	gyear = start + year - 1;

	// 月が1～12の範囲内でなければエラー
	if (month < 1 || month > 12) return INVALID_MONTH;

	// 日が1未満ならエラー
	if (day < 1) return INVALID_DAY;
	if (month == 2 && isLeapYear(year)) {
		// うるう年のとき2月29日以降を指定されていたらエラー
		if (day > 29) return INVALID_DAY;
	} else {
		// うるう年の2月以外で1月当たりの日数がおかしければエラー
		if (day > dpm[month-1]) return INVALID_DAY;
	}

	return gyear;
}

// getNengoString関数	年号の記号から年号名の文字列を返す
// 返り値: 0　　: エラー(年号の記号が不正)
// 　　　  0以外: 年号名
// 引数:   nengo: 年号
const char* getNengoString(char nengo) {
	static const char* names[4] = {"明治", "大正", "昭和", "平成"};
	switch (nengo) {
		case NENGO_MEIJI:  return names[0];
		case NENGO_TAISHO: return names[1];
		case NENGO_SHOWA:  return names[2];
		case NENGO_HEISEI: return names[3];
	}
	return 0;
}

int main() {
	char nengo; // 年号の記号
	int gyear;  // 西暦
	int year, month, day;   // 和暦、月、日

	printf("和暦と年月日を半角スペース区切りで入力してください。\n");
	printf("年号は %c:明治 %c:大正 %c:昭和 %c:平成 を使用してください。\n",
		NENGO_MEIJI, NENGO_TAISHO, NENGO_SHOWA, NENGO_HEISEI);
	printf("入力例: H 25 05 09\n");
	printf("? ");
	scanf("%c %d %d %d", &nengo, &year, &month, &day);

	switch (gyear = getYear(uppercase(nengo), year, month, day)) {
		case INVALID_NENGO: puts("年号がおかしいです。"); break;
		case INVALID_YEAR:  puts("年がおかしいです。");   break;
		case INVALID_MONTH: puts("月がおかしいです。");   break;
		case INVALID_DAY:   puts("日付がおかしいです。"); break;
		default:
			if (year == 1)
				printf("%s元年%2d月%2d日 は 西暦%d年%2d月%2d日 です。\n",
					getNengoString(nengo), month, day, gyear, month, day);
			else
				printf("%s%d年%2d月%2d日 は 西暦%d年%2d月%2d日 です。\n",
					getNengoString(nengo), year, month, day, gyear, month, day);
			break;
	}

	return 0;
}

// 実行結果
/*
和暦と年月日を半角スペース区切りで入力してください。
年号は M:明治 T:大正 S:昭和 H:平成 を使用してください。
入力例: H 25 05 09
? H 25 5 30
平成25年 5月30日 は 西暦2013年 5月30日 です。
*/

