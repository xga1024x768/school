// 実習課題43 BMIの計算

#include <stdio.h>

double bmi(double, double);
double standardW(double);
void obesity(double);

int main()
{
    double weight, height;      // それぞれ入力された体重と身長

	printf("体重(kg)と身長(cm)を入力してください: ");
	while (scanf("%lf %lf", &weight, &height) != EOF)
	{
		// cmからmにする
		height = height / 100;

		// BMI / 標準体重表示
		printf("BMI: %.2lf / 標準体重: %.2lfkg\n", bmi(weight, height), standardW(height));

		// 肥満度表示
		printf("あなたは");
		obesity(bmi(weight, height));
		printf("です。\n");

		// 標準入力をクリアしておく
		rewind(stdin);

		// 次のプロンプト表示
		printf("体重(kg)と身長(cm)を入力してください: ");
	}
}

// BMIの計算
// 返り値: BMI
// weight: 体重 (kg)
// height: 身長 (m)
double bmi(double weight, double height)
{
	return weight / height / height;
}

// 標準体重の計算
// 返り値: 標準体重
// height: 身長 (m)
double standardW(double height)
{
	return 22 * height * height;
}

// 肥満度の表示
// 返り値: なし
// bmi: BMI
void obesity(double bmi)
{
	if (bmi < 18.5)
	{
		printf("低体重");
	}
	else if (bmi < 25)
	{
		printf("普通体重");
	}
	else if (bmi < 30)
	{
		printf("肥満1度");
	}
	else if (bmi < 35)
	{
		printf("肥満2度");
	}
	else if (bmi < 40)
	{
		printf("肥満3度");
	}
	else
	{
		printf("肥満4度");
	}
}

